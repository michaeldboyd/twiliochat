
var accessToken;
var userId;
var channelId;

function setChannelId(channelId) {
  this.channelId = channelId;
}

function getChannelId() {
  return channelId;
}

function setUserId(userId) {
  this.userId = userId;
}

function getUserId() {
	return userId;
}

function setAccessToken(token) {
  this.accessToken = token;
}

function getAccessToken() {
  return accessToken;
}

