define({ 

  onNavigate : function(context, isBackNavigation) {
    var userId = context.userId;
    setUserId(userId);
    if(context.channelId !== undefined && context.channelId !== null) {
      setChannelId(context.channelId);
    }
  }, 
  
  initChatData:function() {
    var serviceName = "TokenGeneratorService";
    var integrationObj = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);

	userId = getUserId();
    var operationName =  "getTwilioAccessToken";
    var data= {"userId": userId};
    var headers= {};
    integrationObj.invokeOperation(operationName, headers, data, 
         this.getAccessTokenSuccess.bind(this), this.getAccessTokenFailure.bind(this));     
  },
  
  getAccessTokenSuccess : function(response) {
    var channelId = getChannelId();
    //alert(channelId);
    var token = response.accessToken;
    var initializeChat = "initializeChat('" + token + "','" + userId + "','" + channelId + "')";
    
    //var initializeChat = "initializeChat('" + token + "','" + userId + "')";
    
    //alert("calling initialingChat from controller");
    
    //alert("execute initialzeChat: " + initializeChat);
    //this.view.chatBrowser.evaluateJavaScript(initializeChat);
    
    this.view.chatBrowser.evaluateJavaScript(initializeChat);
    
  },
  
  navigateToHome : function() {
      kony.application.destroyForm("frmChat");
      var navToChat = new kony.mvc.Navigation("frmHome");
      navToChat.navigate();  
  },
  

  getAccessTokenFailure : function(response) {
	alert("Unable to fetch token");
  },
  
  sendMessage : function() {
    var input = this.view.txtInput.text;
    if(input !== '') {
      var sendMessage = "sendMessageFromKony('" + input + "')";
      this.view.chatBrowser.evaluateJavaScript(sendMessage);
    }
    this.view.txtInput.text = '';
  }

});