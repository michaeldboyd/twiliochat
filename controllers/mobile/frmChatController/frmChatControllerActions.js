define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBack **/
    AS_Button_a7f488fa43c4442db6aa32c6233ae209: function AS_Button_a7f488fa43c4442db6aa32c6233ae209(eventobject) {
        var self = this;
        return self.navigateToHome.call(this);
    },
    /** onClick defined for btnSend **/
    AS_Button_d75d412196ba4a3087a31c2e4c789155: function AS_Button_d75d412196ba4a3087a31c2e4c789155(eventobject) {
        var self = this;
        return self.sendMessage.call(this);
    },
    /** postShow defined for frmChat **/
    AS_Form_id19c58cc9444f6893e589a416f8ad5e: function AS_Form_id19c58cc9444f6893e589a416f8ad5e(eventobject) {
        var self = this;
        return self.initChatData.call(this);
    }
});