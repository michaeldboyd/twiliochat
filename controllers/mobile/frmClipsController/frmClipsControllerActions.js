define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segClips **/
    AS_Segment_c7a939661e5f4bc1af9aca588cb6a981: function AS_Segment_c7a939661e5f4bc1af9aca588cb6a981(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.navigateToChat.call(this);
    },
    /** onClick defined for btnBack **/
    AS_Button_h53f8814f9574d7dac89dc72a454ca72: function AS_Button_h53f8814f9574d7dac89dc72a454ca72(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmHome");
        ntf.navigate();
    }
});