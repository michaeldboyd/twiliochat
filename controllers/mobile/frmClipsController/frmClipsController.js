define({ 

  onNavigate : function(context, isBackNavigation) {

    var userId = context.userId;
    setUserId(userId);
    var serviceName = "TokenGeneratorService";
    var integrationObj = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);

    var operationName =  "getTwilioAccessToken";
    var data= {"userId": userId};
    var headers= {};
    integrationObj.invokeOperation(operationName, headers, data, 
         this.getAccessTokenSuccess.bind(this), this.getAccessTokenFailure.bind(this));    
    
  },
  
  getAccessTokenSuccess : function(response) {
    var token = response.accessToken;
    setAccessToken(token);
  },
  
  getAccessTokenFailure : function(response) {
	alert("Unable to fetch token");
  }, 
  
  navigateToChat : function() {

    var selectedData = null;
    var clipId = null;

    if(this.view.segClips.selectedRowItems !== null) 
    {
      selectedData = this.view.segClips.selectedRowItems[0];
      clipId = selectedData.clipId;
      var paramData = {"channelId" : clipId, "userId" : getUserId() };
      var navToForm = new kony.mvc.Navigation("frmChat");
      navToForm.navigate(paramData);  
    }   

  },  
  
 });