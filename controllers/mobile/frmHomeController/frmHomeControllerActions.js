define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lstReferees **/
    AS_ListBox_c760005baf1942dfb2c193ea634ee01c: function AS_ListBox_c760005baf1942dfb2c193ea634ee01c(eventobject) {
        var self = this;
        this.view.btnChat.setEnabled(true);
    },
    /** onClick defined for btnChat **/
    AS_Button_f72f42c258bd4f82bc65371ba90733b2: function AS_Button_f72f42c258bd4f82bc65371ba90733b2(eventobject) {
        var self = this;
        return self.navigateToGeneralChat.call(this);
    },
    /** onClick defined for btnClips **/
    AS_Button_gd57ae140ca1416cb17b3203d038ca24: function AS_Button_gd57ae140ca1416cb17b3203d038ca24(eventobject) {
        var self = this;
        return self.navigateToClips.call(this);
    }
});