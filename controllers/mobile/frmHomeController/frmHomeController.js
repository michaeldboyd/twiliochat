define({ 

  //Type your controller code here 


  onNavigate : function(context, isBackNavigation) {

    //this.view.btnChat.setEnabled(false);

    var serviceName = "NBAObjectServices";
    var serviceType = "online";
    var objSvc = kony.sdk.getCurrentInstance().getObjectService(serviceName, {"access": serviceType});

    var objectName = "Referee";
    var dataObject = new kony.sdk.dto.DataObject(objectName); 
    var options = {"dataObject":dataObject, "queryParams": {"leagueId": "00"}};
    objSvc.fetch(options, this.categoryFetchSuccessCallback.bind(this), this.categoryFetchFailureCallback.bind(this));

  },

  categoryFetchSuccessCallback: function(response) {
    var referees = response.records[0].Referees;
    var refereeListMasterData = [];
    for(var i = 0; i < referees.length; i++) {
      refereeListMasterData.push([referees[i].userId, referees[i].firstName + " " + referees[i].lastName]);
    }
    this.view.lstReferees.masterData = refereeListMasterData;
  },

  categoryFetchFailureCallback : function(response) {
    alert("Unable to fetch Referees"); 
  },
  
  navigateToGeneralChat : function() {
    var userId = null;
    if(this.view.lstReferees.selectedKey !== null) {
      userId = this.view.lstReferees.selectedKey;
      var paramData = {"userId" : userId };
      var navToChat = new kony.mvc.Navigation("frmChat");
      navToChat.navigate(paramData);  

     }
  },
  
  navigateToClips : function() {
    var userId = null;
    if(this.view.lstReferees.selectedKey !== null) {
      userId = this.view.lstReferees.selectedKey;
      var paramData = {"userId" : userId };
      var navToChat = new kony.mvc.Navigation("frmClips");
      navToChat.navigate(paramData);  

     }
  },
  
});